package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

/**
 * Created by Sarah McLaughlin on 11/18/16
 */
@Autonomous (name = "Drive Forward", group = "Autonomouses")
public class Auto111816 extends LinearOpMode{

    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    public void runOpMode() throws InterruptedException{

        //configure hardware map
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront.setDirection(DcMotor.Direction.REVERSE);
        rightback.setDirection(DcMotor.Direction.REVERSE);

        waitForStart();

        sleep(15000);

        //go
        leftfront.setPower(0.5);
        leftback.setPower(0.5);
        rightfront.setPower(0.5);
        rightback.setPower(0.5);
        sleep(3000);

        //stop
        leftfront.setPower(0);
        leftback.setPower(0);
        rightfront.setPower(0);
        rightback.setPower(0);
    }

}
