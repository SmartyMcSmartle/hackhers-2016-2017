package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;

/**
 * Created by Sarah McLaughlin on 12/9/16
 */

@Autonomous (name = "Blue Full Autonomous", group = "Autonomouses")
public class BlueFullAuto extends LinearOpMode {

    MultiplexerDevice muxcolor;
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;
    DcMotor ballflicker;
    DcMotor cam;
    Servo leftpusher;
    Servo rightpusher;
    Servo stopper;

    TouchSensor ts;
    OpticalDistanceSensor odsl;
    OpticalDistanceSensor odsr;

    static final double Max_pos = Servo.MAX_POSITION; // Maximum position
    static final double Min_pos = 0.3; // Minimum position

    int[] ports = {0, 1, 2, 3};

    @Override
    public void runOpMode() throws InterruptedException {
        leftback = hardwareMap.dcMotor.get("leftback");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftpusher = hardwareMap.servo.get("leftpusher");
        rightpusher = hardwareMap.servo.get("rightpusher");
        ballflicker = hardwareMap.dcMotor.get("ballflicker");
        cam = hardwareMap.dcMotor.get("cam");
        stopper = hardwareMap.servo.get("stopper");
        ts = hardwareMap.touchSensor.get("touchsensor");

        odsl = hardwareMap.opticalDistanceSensor.get("odsl");  //port 7
        odsr = hardwareMap.opticalDistanceSensor.get("odsr");  //port 6

        //Software robot motor reverse
        //leftfront.setDirection(DcMotorSimple.Direction.REVERSE); //comment out on hardware robot
        //leftback.setDirection(DcMotorSimple.Direction.REVERSE); //comment out on hardware robot
        //rightback.setDirection(DcMotorSimple.Direction.REVERSE); //comment out on hardware robot

        //Hardware robot motor reverse
        rightfront.setDirection(DcMotorSimple.Direction.REVERSE); //*** NEED TO UNCOMMENT ON HARDWARE ROBOT***///
        rightback.setDirection(DcMotorSimple.Direction.REVERSE); //*** NEED TO UNCOMMENT ON HARDWARE ROBOT***///

        cam.setDirection(DcMotorSimple.Direction.REVERSE);

        int milliSeconds = 48;
        muxcolor = new MultiplexerDevice(hardwareMap, "mux", "ada", ports, milliSeconds,
                MultiplexerDevice.GAIN_16X);

        //threshold values for 48 milliSeconds
        int upperbound = SensorVal.hwhitebound;
        int redlowerbound0 = SensorVal.hredbound0;
        int redlowerbound1 = SensorVal.hredbound1;
        int redlowerbound2 = SensorVal.hredbound2;
        int bluelowerbound0 = SensorVal.hbluebound0;
        int bluelowerbound1 = SensorVal.hbluebound1;
        int bluelowerbound2 = SensorVal.hbluebound2;
        double leftods = SensorVal.left4;
        double rightods = SensorVal.right4;

        double speed = SensorVal.speed13;
        double slow = SensorVal.slow13;

        int drivetime = 1750; //time to drive forward in first part of program, in milliseconds; ***NEEDS TO BE TESTED***

        stopper.setPosition(0.5);
        leftpusher.setPosition(0.3);
        rightpusher.setPosition(0.3);

        //start the program
        waitForStart();
        muxcolor.startPolling();

        //create an array of the different color sensor values
        int[] crgb0;  //left
        int[] crgb1;  //right
        int[] crgb2;  //left beacon

        if(opModeIsActive()) {
            drive(-speed);
            sleep(drivetime);
            drive(0);
        }

        //update color sensor values
        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        //Finding and straightening the robot on the white line
        if(!isWhite(crgb0,upperbound) || !isWhite(crgb1,upperbound)){ //if both color sensors don't see white
            while((!isWhite(crgb0,upperbound) || !isWhite(crgb1,upperbound)) && opModeIsActive()){ //while the sensors don't both see white
                telemetry.addData("RGB 0", "%5d %5d %5d", crgb0[1], crgb0[2], crgb0[3]);
                telemetry.addData("RGB 1", "%5d %5d %5d", crgb1[1], crgb1[2], crgb1[3]);
                telemetry.update();
                crgb0 = muxcolor.getCRGB(ports[0]); //update
                crgb1 = muxcolor.getCRGB(ports[1]);
                if(isWhite(crgb1,upperbound)){ //if sensor 0 sees white
                    while(isWhite(crgb1,upperbound) && !isWhite(crgb0,upperbound) && opModeIsActive()){ //while sensor 0 sees white
                        leftfront.setPower(-1.5*slow); //turn
                        leftback.setPower(-1.5*slow);
                        rightfront.setPower(3*slow);
                        rightback.setPower(3*slow);
                        /*leftfront.setPower(3*slow);
                        leftfront.setPower(3*slow);
                        rightfront.setPower(-3*slow);
                        rightback.setPower(-3*slow);*/
                        telemetry.addData("RGB 0", "%5d %5d %5d", crgb0[1], crgb0[2], crgb0[3]);
                        telemetry.addData("RGB 1", "%5d %5d %5d", crgb1[1], crgb1[2], crgb1[3]);
                        telemetry.update();
                        crgb0 = muxcolor.getCRGB(ports[0]); //update
                        crgb1 = muxcolor.getCRGB(ports[1]); //update
                    }
                    /*crgb0 = muxcolor.getCRGB(ports[0]); //update
                    crgb1 = muxcolor.getCRGB(ports[1]); //update
                    if((!isWhite(crgb0,upperbound) || !isWhite(crgb1,upperbound)) && opModeIsActive()) {
                        telemetry.addLine("Going backwards");
                        telemetry.update();
                        drive(speed / 1.5);
                        sleep(400);
                        drive(0);
                    }*/
                    crgb0 = muxcolor.getCRGB(ports[0]); //update
                    crgb1 = muxcolor.getCRGB(ports[1]); //update
                } else if(!isWhite(crgb1,upperbound)){ //otherwise, if sensor 0 doesn't see white
                    while(!isWhite(crgb1,upperbound) && opModeIsActive()){
                        drive(-0.07); //drive toward white line (backwards)
                        telemetry.addData("RGB 0", "%5d %5d %5d", crgb0[1], crgb0[2], crgb0[3]);
                        telemetry.addData("RGB 1", "%5d %5d %5d", crgb1[1], crgb1[2], crgb1[3]);
                        telemetry.update();
                        crgb1 = muxcolor.getCRGB(ports[1]); //update
                        crgb0 = muxcolor.getCRGB(ports[0]); //update
                    }
                }
            }
        }

        drive(0);


        //drive sideways to the left until the robot is the correct distance from the beacon
        if((1000*odsl.getRawLightDetected()) < leftods){
            while((1000*odsl.getRawLightDetected()) < leftods && opModeIsActive()){
                telemetry.addData("left ods", 1000*odsl.getRawLightDetected());
                telemetry.update();
                leftfront.setPower(-speed);
                rightfront.setPower(speed);
                leftback.setPower(speed);
                rightback.setPower(-speed);
            }
        }
        drive(0);

        //drives forwards until the robot doesn't see the beacon
        if((1000*odsl.getRawLightDetected()) > leftods){
            while((1000*odsl.getRawLightDetected()) > leftods && opModeIsActive()){
                telemetry.addData("left ods", 1000 * odsl.getRawLightDetected());
                telemetry.update();
                drive(2 * slow);
            }
        }
        drive(0);

        //update color sensor
        crgb2 = muxcolor.getCRGB(ports[2]);

        //drive backwards until left beacon color sensor sees blue
        if(isRed(crgb2,redlowerbound2,upperbound) || isGrey(crgb2,redlowerbound2,bluelowerbound2,upperbound)){
            while((isRed(crgb2,redlowerbound2,upperbound) || isGrey(crgb2,redlowerbound2,bluelowerbound2,upperbound)) && opModeIsActive()){
                telemetry.addLine("Looking for red");
                telemetry.update();
                crgb2 = muxcolor.getCRGB(ports[2]);
                drive(-1.5*slow);
            }
        }
        drive(0);

        //line up to the button
        if(opModeIsActive()) {
            drive(speed);
            sleep(150);
            drive(0);
        }

        //push button
        if(opModeIsActive()) {
            telemetry.addLine("Pushing beacon");
            telemetry.update();
            leftpusher.setPosition(Max_pos);
            sleep(2000);
            leftpusher.setPosition(Min_pos);
        }

        //drive sideways to the right until the robot is far enough away from the beacon
        if((1000*odsl.getLightDetected()) > leftods){
            while((1000*odsl.getRawLightDetected()) > leftods && opModeIsActive()){
                telemetry.addData("left ods", 1000*odsl.getRawLightDetected());
                telemetry.update();
                leftfront.setPower(speed);
                rightfront.setPower(-speed);
                leftback.setPower(-speed);
                rightback.setPower(speed);
            }
        }
        drive(0);

        //drive backwards for a certain time
        if(opModeIsActive()){
            drive(-speed);
            sleep(1700);
        }

        drive(0);

        //update color sensor values
        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        telemetry.addData("RGB 0", "%5d %5d %5d", crgb0[1], crgb0[2], crgb0[3]);
        telemetry.addData("RGB 1", "%5d %5d %5d", crgb1[1], crgb1[2], crgb1[3]);
        telemetry.update();

        sleep(500);

        //drive forward until one sensor sees a color
        if(isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound)){
            while(isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound) && opModeIsActive()){
                telemetry.addLine("I see grey");
                telemetry.update();
                crgb0 = muxcolor.getCRGB(ports[0]);
                crgb1 = muxcolor.getCRGB(ports[1]);
                drive(-1.5*slow);
            }
        } else {
            drive(0);
        }

        //drive sideways to the left until the robot is close enough to the beacon or right until the robot is far enough away from the beacon
        if((1000*odsl.getLightDetected()) < leftods){
            while((1000*odsl.getRawLightDetected()) < leftods && opModeIsActive()){
                telemetry.addData("left ods", 1000*odsl.getRawLightDetected());
                telemetry.update();
                leftfront.setPower(-speed);
                rightfront.setPower(speed);
                leftback.setPower(speed);
                rightback.setPower(-speed);
            }
        } else if((1000*odsl.getLightDetected()) > leftods){
            while((1000*odsl.getRawLightDetected()) > leftods && opModeIsActive()){
                telemetry.addData("left ods", 1000*odsl.getRawLightDetected());
                telemetry.update();
                leftfront.setPower(speed);
                rightfront.setPower(-speed);
                leftback.setPower(-speed);
                rightback.setPower(speed);
            }
        }
        drive(0);

        //drive forwards until the robot doesn't see the beacon
        if((1000*odsl.getRawLightDetected()) > leftods){
            while((1000*odsl.getRawLightDetected()) > leftods && opModeIsActive()) {
                telemetry.addData("left ods", 1000*odsl.getRawLightDetected());
                telemetry.update();
                drive(2*slow);
            }
        }
        drive(0);

        //update color sensor
        crgb2 = muxcolor.getCRGB(ports[2]);

        //drive until the left beacon color sensor sees blue
        if(isRed(crgb2,redlowerbound2,upperbound) || isGrey(crgb2,redlowerbound2,bluelowerbound2,upperbound)){
            while((isRed(crgb2,redlowerbound2,upperbound) || isGrey(crgb2,redlowerbound2,bluelowerbound2,upperbound)) && opModeIsActive()){
                crgb2 = muxcolor.getCRGB(ports[2]);
                drive(-1.5*slow);
            }
        }
        drive(0);

        //line up to button
        if(opModeIsActive()) {
            drive(speed);
            sleep(100);
            drive(0);
        }

        //double check the robot is at the correct distance
        if((1000*odsl.getLightDetected()) < leftods){
            while((1000*odsl.getRawLightDetected()) < leftods && opModeIsActive()){
                telemetry.addData("left ods", 1000*odsl.getRawLightDetected());
                telemetry.update();
                leftfront.setPower(-speed);
                rightfront.setPower(speed);
                leftback.setPower(speed);
                rightback.setPower(-speed);
            }
        }

        drive(0);

        //push button
        if(opModeIsActive()) {
            leftpusher.setPosition(Max_pos);
            sleep(1500);
            leftpusher.setPosition(Min_pos);
        }

        //turn the robot
        if(opModeIsActive()){
            rightfront.setPower(1.5*speed);
            rightback.setPower(1.5*speed);
            leftfront.setPower(-1.5*speed);
            leftback.setPower(-1.5*speed);
            sleep(1400);
            drive(0);
        }

        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        //go backwards until robot sees a color
        if(isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound)){
            while (opModeIsActive() && isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound)) {
                crgb0 = muxcolor.getCRGB(ports[0]);
                crgb1 = muxcolor.getCRGB(ports[1]);
                drive(-speed);
            }
        }

        drive(0);

        if(opModeIsActive()){
            drive(speed);
            sleep(600);
        }
        drive(0);

        //shoots particles
        if(opModeIsActive()) {
            //moves cam while touch sensor is pressed
            if (!ts.isPressed()) {
                while (!ts.isPressed() && opModeIsActive()) {
                    cam.setPower(1);
                }
            }
            cam.setPower(0);
            stopper.setPosition(0.0);

            //moves ballflicker while touch sensor isn't pressed
            if (ts.isPressed()) {
                while (ts.isPressed() && opModeIsActive()) {
                    ballflicker.setPower(.9);
                }
            }
            sleep(500);
            ballflicker.setPower(0);
            stopper.setPosition(0.5);

            //moves cam while touch sensor is pressed
            if (!ts.isPressed()) {
                while (!ts.isPressed() && opModeIsActive()) {
                    cam.setPower(1);
                }
            }
            cam.setPower(0);

            //drives backwards to partially park on center
            drive(-speed);
            sleep(600);
            drive(0);
        }
    }

    /**
     * Has the robot move all of its motors at one speed. It cannot turn, but it can move fowards, backwards, and stop.
     *
     * @param speed    desired speed of the motors
     */
    public void drive(double speed){
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
    }

    /**
     * Based on predetermined bounds for the color red, decides if the color sensor is sensing a red line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color red
     * @param higherbound    predetermined higher bound for the color red
     */
    public boolean isRed(int[] colorarray, int lowerbound, int higherbound){
        return(colorarray[1] > lowerbound && colorarray[1] < higherbound);
    }

    /**
     * Based on predetermined bounds for the color blue, decides if the color sensor is sensing a blue line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color blue
     * @param higherbound    predetermined higher bound for the color blue
     */
    public boolean isBlue(int[] colorarray, int lowerbound, int higherbound){
        return(colorarray[3] > lowerbound && colorarray[3] < higherbound);
    }

    /**
     * Based on predetermined bounds for the color white, decides if the color sensor is sensing a white line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param bound    predetermined lower bound for the color white (since white is the highest color we sensed, we have no need for a higher bound)
     */
    public boolean isWhite(int[] colorarray, int bound){
        return(colorarray[1] > bound || colorarray[2] > bound || colorarray[3] > bound);
    }

    /**
     * Based on whether it is seeing non-grey colors. If it is, then it is not grey. Otherwise, it is grey.
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerboundred    predetermined lower bound for the color red
     * @param lowerboundblue    predetermined lower bound for the color blue
     * @param whitebound    predetermined higher bound for the colors, which is also the bound for white
     */
    public boolean isGrey(int[] colorarray, int lowerboundred, int lowerboundblue, int whitebound){
        return(!isRed(colorarray,lowerboundred,whitebound) &&
                !isBlue(colorarray,lowerboundblue,whitebound) &&
                !isWhite(colorarray,whitebound));
    }
}