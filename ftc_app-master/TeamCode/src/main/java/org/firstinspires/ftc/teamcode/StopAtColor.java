package org.firstinspires.ftc.teamcode;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DeviceInterfaceModule;
import com.qualcomm.robotcore.hardware.DigitalChannelController;
import com.qualcomm.robotcore.hardware.LED;

import java.nio.channels.Channel;

/*
 * Created by Sarah, Audrey, and Shih-Ting on 11/29/16
 */

@Autonomous (name = "Stop At Color (One Sensor)", group = "Autonomouses")
public class StopAtColor extends LinearOpMode {

    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    ColorSensor colorsensor;
    //ColorSensor colorsensorl;

    DeviceInterfaceModule cdim;

    public void runOpMode() throws InterruptedException {

        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        colorsensor = hardwareMap.colorSensor.get("colorsensor");
        //colorsensorr = hardwareMap.colorSensor.get("colorsensorright");
        cdim = hardwareMap.deviceInterfaceModule.get("cdim");
        leftfront.setDirection(DcMotorSimple.Direction.REVERSE);
        leftback.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        float hsvValues[] = {0F, 0F, 0F};
        //float hsvValuesR[] = {0F, 0F, 0F};

        int LED_CHANNEL_L =5;
        int LED_CHANNEL_R =4;

        cdim.setDigitalChannelMode(LED_CHANNEL_L, DigitalChannelController.Mode.OUTPUT);
        cdim.setDigitalChannelMode(LED_CHANNEL_R, DigitalChannelController.Mode.OUTPUT);

        cdim.setDigitalChannelState(LED_CHANNEL_L, true);
        cdim.setDigitalChannelState(LED_CHANNEL_R, true);

        Color.RGBToHSV((colorsensor.red() * 255) / 800, (colorsensor.green() * 255) / 800, (colorsensor.blue() * 255) / 800, hsvValues);
        //Color.RGBToHSV((colorsensorr.red() * 255) / 800, (colorsensorr.green() * 255) / 800, (colorsensorr.blue() * 255) / 800, hsvValuesL);

        telemetry.addData("Hue", hsvValues[0]);
        //telemetry.addData("Hue Right", hsvValuesR[0]);
        telemetry.update();

        int redgrey = 15; //threshold between red and grey, red is below and gray is above
        int greyblue = 80; //threshold between grey and blue, gray is below, blue is above
        int bluered = 225; //threshold between blue and red, blue is below, red is above through 360 (and then wraps back to 0)
        //need thresholds for white values

        double speed = -0.05;

        waitForStart();

        while(hsvValues[0] > redgrey && hsvValues[0] < greyblue /*&& hsvValuesR[0] > redgrey && hsvValuesR[0] < greyblue*/){
            Color.RGBToHSV((colorsensor.red() * 255)/800, (colorsensor.green()*255)/800, (colorsensor.blue() * 255)/800, hsvValues);
            //Color.RGBToHSV((colorsensorr.red() * 255)/800, (colorsensorr.green()*255)/800, (colorsensorr.blue() * 255)/800, hsvValuesL);
            telemetry.addData("Hue", hsvValues[0]);
            //telemetry.addData("Hue Right", hsvValuesR[0]);
            telemetry.update();

            rightback.setPower(speed);
            leftback.setPower(speed);
            rightfront.setPower(speed);
            leftfront.setPower(speed);
        }

        rightback.setPower(0);
        leftback.setPower(0);
        rightfront.setPower(0);
        leftfront.setPower(0);
    }
}
