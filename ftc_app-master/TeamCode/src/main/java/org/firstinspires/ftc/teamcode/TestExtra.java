package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/*
 * Created by sarahmclaughlin on 11/15/16.
 */
public class TestExtra extends LinearOpMode {

    DcMotor leftfront;

    @Override
    public void runOpMode() throws InterruptedException {
        leftfront = hardwareMap.dcMotor.get("leftfront");

        waitForStart();

        leftfront.setPower(1.0);
        sleep(500);
        leftfront.setPower(0);
    }
}
