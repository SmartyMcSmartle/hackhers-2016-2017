package org.firstinspires.ftc.teamcode;

/*
 * This class will store the sensor values that we want to use for all of our autonomous programs
 * Created by Sarah McLaughlin on 1/31/17
 */
public class SensorVal {
    //Color Sensor values
    //hardware bounds
    public static int hwhitebound = 9000;
    public static int hredbound0 = 4000;
    public static int hredbound1 = 4300;
    public static int hredbound2 = 1100;
    public static int hbluebound0 = 4000;
    public static int hbluebound1 = 4000;
    public static int hbluebound2 = 1500;

    //software bounds
    public static int swhitebound = 8000;
    public static int sredbound0 = 3000;
    public static int sredbound1 = 3000;
    public static int sredbound2 = 1600;
    public static int sbluebound0 = 3300;
    public static int sbluebound1 = 3700;
    public static int sbluebound2 = 1800;

    //ODS values
    public static double left4 = 130;
    public static double right4 = 150;

    //software robot speeds  ***COMMENT OUT FOR HARDWARE ROBOT***
    public static double speed_software = 0.25;
    public static double slow_software = 0.045;

    //speed at 13.7 volts
    public static double speed13_7 = 0.35;
    public static double slow13_7 = 0.045;

    //speed at 13.5 volts - 13.6 volts
    public static double speed13_5 = 0.35;
    public static double slow13_5 = 0.05;

    //speed at 13.0 - 13.1 volts
    public static double speed13 = 0.35;
    public static double slow13 = 0.06;

    //speed at 12.7 - 12.9 volts
    public static double speed12_7 = 0.35;
    public static double slow12_7 = 0.065;

    //speed at 12.5-12.7 volts
    public static double speed12_5 = 0.5;
    public static double slow12_5 = 0.07;

    //speed at 12.1 - 12.5 volts
    public static double speed12_1 = 0.5;  //needs to be tested
    public static double slow12_1 = 0.0725;  //needs to be tested
}
