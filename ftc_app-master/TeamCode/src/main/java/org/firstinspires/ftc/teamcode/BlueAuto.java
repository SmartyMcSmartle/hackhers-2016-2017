package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

/**
 * Created by Sarah McLaughlin on 12/9/16
 */
public class BlueAuto extends LinearOpMode{

    MultiplexerDevice muxcolor;
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    int[] ports = {0,1,2};

    @Override
    public void runOpMode() throws InterruptedException {
        leftback = hardwareMap.dcMotor.get("leftback");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront = hardwareMap.dcMotor.get("rightfront");

        //Software robot motor reverse
        //leftfront.setDirection(DcMotorSimple.Direction.REVERSE);
        //leftback.setDirection(DcMotorSimple.Direction.REVERSE);
        //rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        /*Hardware robot motor reverse*/
        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);

        int milliSeconds = 48;
        muxcolor = new MultiplexerDevice(hardwareMap, "mux", "ada", ports, milliSeconds,
                MultiplexerDevice.GAIN_16X);

        //threshold values for software robot
        /*int upperbound = 2250; //upper bound for any color, lower bound for white
        int redlowerbound = 1000;
        int bluelowerbound0 = 900;
        int bluelowerbound1 = 700;
        int blackbound = 500;*/

        //threshold values for hardware robot
        int upperbound = 1400; //upper bound for any color, lower bound for white
        int redlowerbound0 = 900;
        int redlowerbound1 = 900;
        int bluelowerbound = 900;
        int blackbound = 0;

        //robot speed
        double speed = 0.25;

        //software robot speed change
        //double speedChange = 0.008;

        //hardware robot speed change
        double speedChange = 0.009;

        waitForStart();

        sleep(9000);
        muxcolor.startPolling();

        //create an array of the different crgb values
        int[] crgb0 = muxcolor.getCRGB(ports[0]);
        int[] crgb1 = muxcolor.getCRGB(ports[1]);
        int[] crgb2 = muxcolor.getCRGB(ports[2]);

        //drive forward until one sensor sees a color
        if(isGrey(crgb0,redlowerbound0,bluelowerbound,upperbound,blackbound) && isGrey(crgb1,redlowerbound1,bluelowerbound,upperbound,blackbound)){
            while(isGrey(crgb0,redlowerbound0,bluelowerbound,upperbound,blackbound) && isGrey(crgb1,redlowerbound1,bluelowerbound,upperbound,blackbound)) {
                crgb0 = muxcolor.getCRGB(ports[0]);
                crgb1 = muxcolor.getCRGB(ports[1]);
                drive(-speed);
            }
        } else {
            drive(0);
        }

        drive(0);

        crgb1 = muxcolor.getCRGB(ports[1]);

        //turn forwards to the left
        if(!isBlue(crgb1, bluelowerbound, upperbound)){
            telemetry.addLine("I don't see blue");
            telemetry.update();
            sleep(100);
            while(!isBlue(crgb1, bluelowerbound, upperbound)){
                crgb1 = muxcolor.getCRGB(ports[1]);
                leftfront.setPower(speed);
                leftback.setPower(speed);
                rightfront.setPower(-speed);
                rightback.setPower(-speed);
            }
        } else {
            drive(0);
            telemetry.addLine("I see blue");
            telemetry.update();
            sleep(100);
        }

        drive(0);

        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        //drive backwards while line following until color sensor senses black center platform
        if(!isBlue(crgb0,bluelowerbound,upperbound)){
            while(!isBlue(crgb0,bluelowerbound,upperbound)){   //while the left color sensor doesn't see blue
                telemetry.addLine("Looking for black");
                telemetry.update();
                crgb0 = muxcolor.getCRGB(ports[0]);
                crgb1 = muxcolor.getCRGB(ports[1]);
                drive(-0.1);
                if(!isBlue(crgb1, bluelowerbound, upperbound)){   //while the color sensor is not on blue
                    telemetry.addLine("Looking for blue");
                    telemetry.update();
                    if(isRed(crgb0, redlowerbound0, upperbound)){  // if it's red, turn right towards blue line
                        while(isBlue(crgb0,bluelowerbound,upperbound)) {
                            telemetry.addLine("See Blue");
                            telemetry.update();
                            crgb0 = muxcolor.getCRGB(ports[0]);
                            crgb1 = muxcolor.getCRGB(ports[1]);
                            leftfront.setPower(changeSpeed(leftfront,speedChange));
                            leftback.setPower(changeSpeed(leftback,speedChange));
                            rightfront.setPower(-changeSpeed(rightfront,-speedChange));
                            rightback.setPower(-changeSpeed(rightback,-speedChange));
                        }
                    } else if (isGrey(crgb1,redlowerbound1,bluelowerbound,upperbound,blackbound)){   //if it's grey, turn left towards blue line
                        while(isGrey(crgb1,redlowerbound1,bluelowerbound,upperbound,blackbound)){
                            telemetry.addLine("See Grey");
                            telemetry.update();
                            crgb0 = muxcolor.getCRGB(ports[0]);
                            crgb1 = muxcolor.getCRGB(ports[1]);
                            leftfront.setPower(changeSpeed(leftfront,-speedChange));
                            leftback.setPower(changeSpeed(leftback,-speedChange));
                            rightfront.setPower(changeSpeed(rightfront,speedChange));
                            rightback.setPower(changeSpeed(rightback,speedChange));
                        }
                    }
                }
            }
        } else {
            telemetry.addLine("Stop");
            telemetry.update();
            drive(0);
        }

        drive(0);

        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(-speed);
        rightback.setPower(-speed);
        sleep(200);
        drive(0);

        drive(-speed);
        sleep(200);
        drive(0);

    }

    /**
     * Has the robot move all of its motors at one speed. It cannot turn, but it can move fowards, backwards, and stop.
     *
     * @param speed    desired speed of the motors
     */
    public void drive(double speed){
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
    }

    /**
     * Changes the motor speed
     *
     * @param motor    DcMotor that needs to change speed
     * @param change   Amount to change motor speed by
     */
    public double changeSpeed(DcMotor motor, double change){
        double speedCurrent = motor.getPower();
        speedCurrent += change;
        return speedCurrent;
    }

    /**
     * Based on predetermined bounds for the color red, decides if the color sensor is sensing a red line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color red
     * @param higherbound    predetermined higher bound for the color red
     */
    public boolean isRed(int[] colorarray, int lowerbound, int higherbound){
        telemetry.update();
        if(colorarray[1] > lowerbound && colorarray[1] < higherbound) return true;
        return false;
    }

    /**
     * Based on predetermined bounds for the color blue, decides if the color sensor is sensing a blue line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color blue
     * @param higherbound    predetermined higher bound for the color blue
     */
    public boolean isBlue(int[] colorarray, int lowerbound, int higherbound){
        telemetry.addData("Blue", colorarray[3]);
        if(colorarray[3] > lowerbound && colorarray[3] < higherbound) return true;
        return false;
    }

    /**
     * Based on predetermined bounds for the color white, decides if the color sensor is sensing a white line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param bound    predetermined lower bound for the color white (since white is the highest color we sensed, we have no need for a higher bound)
     */
    public boolean isWhite(int[] colorarray, int bound){
        if(colorarray[1] > bound || colorarray[2] > bound ||
                colorarray[3] > bound) return true;
        return false;
    }

    /**
     * Based on predetermined bounds for the color black, decides if the color sensor is sensing the black center platform or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param bound    predetermined lower bound for the color black (since black is the lowest color we sensed, we have no need for a lower bound)
     */
    public boolean isBlack(int[] colorarray, int bound){
        if(colorarray[1] < bound && colorarray[3] < bound) return true;
        return false;
    }

    /**
     * Based on whether it is seeing non-grey colors. If it is, then it is not grey. Otherwise, it is grey.
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerboundred    predetermined lower bound for the color red
     * @param lowerboundblue    predetermined lower bound for the color blue
     * @param whitebound    predetermined higher bound for the colors, which is also the bound for white
     * @param blackbound    predetermined lower bound for grey, which is also the bound for black
     */
    public boolean isGrey(int[] colorarray, int lowerboundred, int lowerboundblue, int whitebound, int blackbound){
        if(!isRed(colorarray,lowerboundred,whitebound) &&
                !isBlue(colorarray,lowerboundblue,whitebound) &&
                !isWhite(colorarray,whitebound) &&
                !isBlack(colorarray,blackbound)) return true;
        return false;
    }
}