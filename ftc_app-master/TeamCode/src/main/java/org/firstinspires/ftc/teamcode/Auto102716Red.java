package org.firstinspires.ftc.teamcode;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DeviceInterfaceModule;
import com.qualcomm.robotcore.hardware.DigitalChannelController;

/*
 * Created by Sarah and Anne on 10/27/16
 */

public class Auto102716Red extends LinearOpMode{

    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    ColorSensor colorsensorleft;
    ColorSensor colorsensorright;
    DeviceInterfaceModule cdim;

    static final int LED_CHANNEL_L = 5;
    static final int LED_CHANNEL_R = 4;

    public void runOpMode(){
        //configure hardware map
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        colorsensorleft = hardwareMap.colorSensor.get("colorsensorleft"); //might have to put this later (see AutonomousTest)
        colorsensorright = hardwareMap.colorSensor.get("colorsensorright"); //might have to put this later (see AutonomousTest)
        cdim = hardwareMap.deviceInterfaceModule.get("cdim");

        //configure color sensor
        //hsvValues is an array that will hold the hue, saturation, and value info
        float hsvValuesL[] = {0F,0F,0F};
        float hsvValuesR[] = {0F,0F,0F};

        //color thresholds
        float red = 10;
        float blue = 22;
        float grey = 26;  //left = 47 - 49
        float white = 30;

        //set the digital channel to output mode.
        //remember, the Adafruit sensor is actually two devices.
        //It's an I2C sensor and it's also an LED that can be turned on or off.
        cdim.setDigitalChannelMode(LED_CHANNEL_L, DigitalChannelController.Mode.OUTPUT);
        cdim.setDigitalChannelMode(LED_CHANNEL_R, DigitalChannelController.Mode.OUTPUT);

        //turn the LED on in the beginning, just so user will know that the sensor is active.
        cdim.setDigitalChannelState(LED_CHANNEL_L, true);
        cdim.setDigitalChannelState(LED_CHANNEL_R, true);

        // send the info back to driver station using telemetry function.
        telemetry.addData("Hue", hsvValuesL[0]);
        telemetry.addData("Hue", hsvValuesR[0]);
        telemetry.update();
        sleep(3000);
        telemetry.update();

        //wait for the start button to be pressed
        waitForStart();

        //drive backwards
        // convert the RGB values to HSV values.
        Color.RGBToHSV((colorsensorleft.red() * 255) / 800, (colorsensorleft.green() * 255) / 800, (colorsensorleft.blue() * 255) / 800, hsvValuesL);
        Color.RGBToHSV((colorsensorright.red() * 255) / 800, (colorsensorright.green() * 255) / 800, (colorsensorright.blue() * 255) / 800, hsvValuesR);

        while((hsvValuesL[0] < white && hsvValuesL[0] > blue) || (hsvValuesR[0] < white && hsvValuesR[0] > blue)){
            rightback.setPower(-0.5);
            leftback.setPower(-0.5);
            rightfront.setPower(-0.5);
            leftfront.setPower(-0.5);
        }

        //stop at the line
        rightback.setPower(0);
        leftback.setPower(0);
        rightfront.setPower(0);
        leftfront.setPower(0);

        //move robot to be perpendicular to the red line
        if(hsvValuesL[0] < white && hsvValuesL[0] > blue){
            while(hsvValuesL[0] < white && hsvValuesL[0] > blue){
                leftback.setPower(-0.5);
                leftfront.setPower(-0.5);
            }
            leftback.setPower(0);
            leftfront.setPower(0);
        }else if(hsvValuesR[0] < white && hsvValuesR[0] > blue) {
            while (hsvValuesR[0] < white && hsvValuesR[0] > blue) {
                rightback.setPower(-0.5);
                rightfront.setPower(-0.5);
            }
            rightback.setPower(0);
            rightfront.setPower(0);
        }

        //turn left

        //follow line backwards until seeing the center platform

        //stop

        //backup for a time until the cap ball is knocked off
    }
}
