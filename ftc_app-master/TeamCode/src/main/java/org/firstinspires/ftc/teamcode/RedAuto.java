package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Sarah McLaughlin on 12/9/16
 */
public class RedAuto extends LinearOpMode{

    MultiplexerDevice muxcolor;
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;
    Servo leftpusher;
    Servo rightpusher;
    OpticalDistanceSensor odsl;
    OpticalDistanceSensor odsr;

    static final double Max_pos = Servo.MAX_POSITION; // Maximum position
    static final double Min_pos = 0.3; // Minimum position

    int[] ports = {0, 1, 2, 3};

    @Override
    public void runOpMode() throws InterruptedException {
        leftback = hardwareMap.dcMotor.get("leftback");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftpusher = hardwareMap.servo.get("leftpusher");
        rightpusher = hardwareMap.servo.get("rightpusher");

        odsl = hardwareMap.opticalDistanceSensor.get("odsl");  //port 7
        odsr = hardwareMap.opticalDistanceSensor.get("odsr");  //port 6

        //Software robot motor reverse
        //leftfront.setDirection(DcMotorSimple.Direction.REVERSE); //comment out on hardware robot
        //leftback.setDirection(DcMotorSimple.Direction.REVERSE); //comment out on hardware robot
        //rightback.setDirection(DcMotorSimple.Direction.REVERSE); //comment out on hardware robot

        //Hardware robot motor reverse
        rightfront.setDirection(DcMotorSimple.Direction.REVERSE); //*** NEED TO UNCOMMENT ON HARDWARE ROBOT***///
        rightback.setDirection(DcMotorSimple.Direction.REVERSE); //*** NEED TO UNCOMMENT ON HARDWARE ROBOT***///

        int milliSeconds = 48;
        muxcolor = new MultiplexerDevice(hardwareMap, "mux", "ada", ports, milliSeconds,
                MultiplexerDevice.GAIN_16X);

        //threshold values
        int upperbound = SensorVal.hwhitebound;
        int redlowerbound0 = SensorVal.hredbound0;
        int redlowerbound1 = SensorVal.hredbound1;
        int redlowerbound3 = SensorVal.hredbound2;
        int bluelowerbound0 = SensorVal.hbluebound0;
        int bluelowerbound1 = SensorVal.hbluebound1;
        int bluelowerbound3 = SensorVal.hbluebound2;
        double leftods = SensorVal.left4;
        double rightods = SensorVal.right4;

        ///software robot speeds  ***COMMENT OUT FOR HARDWARE ROBOT***
        double speed_software = 0.25;
        double slow_software = 0.045;

        //speed at 13.0 - 13.1 volts
        double speed13 = 0.35;
        double slow13 = 0.06;

        //speed at 12.7 - 12.9 volts
        double speed12_7 = 0.35;
        double slow12_7 = 0.065;

        //speed at 12.5-12.7 volts
        double speed12_5 = 0.5;
        double slow12_5 = 0.065;

        //speed at 12.2 - 12.5 volts
        double speed12_2 = 0.5;  //needs to be tested
        double slow12_2 = 0.07;  //needs to be tested

        //speed at 12.1 - 12.2 volts
        double speed12_1 = 0.5;
        double slow12_1 = 0.75;

        //robot speed (point to speeds defined above)
        double speed = speed12_7;
        double slow = slow12_7;

        int drivetime = 2000; //time to drive forward in first part of program, in milliseconds; ***NEEDS TO BE TESTED***

        waitForStart();
        muxcolor.startPolling();

        //create an array of the different crgb values
        int[] crgb0;  //left
        int[] crgb1;  //right
        int[] crgb3; //right beacon

        /*
        if(opModeIsActive()) {
            drive(-speed);
            sleep(drivetime);
            drive(0);
        }

        //update color sensor values
        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        //Finding and straightening the robot on the white line
        if(!isWhite(crgb0,upperbound) || !isWhite(crgb1,upperbound)){ //if both color sensors don't see white
            while((!isWhite(crgb0,upperbound) || !isWhite(crgb1,upperbound)) && opModeIsActive()){ //while the sensors don't both see white
                crgb0 = muxcolor.getCRGB(ports[0]); //update
                crgb1 = muxcolor.getCRGB(ports[1]);
                if(isWhite(crgb0,upperbound) && opModeIsActive()){ //if sensor 0 sees white
                    while(isWhite(crgb0,upperbound)){ //while sensor 0 sees white
                        crgb0 = muxcolor.getCRGB(ports[0]); //update
                        crgb1 = muxcolor.getCRGB(ports[1]); //update
                        leftfront.setPower(2*slow); //turn
                        leftback.setPower(2*slow);
                        rightfront.setPower(0);
                        rightback.setPower(0);
                    }
                } else if(!isWhite(crgb0,upperbound)){ //otherwise, if sensor 0 doesn't see white
                    while(!isWhite(crgb0,upperbound) && opModeIsActive()){
                        crgb1 = muxcolor.getCRGB(ports[1]); //update
                        crgb0 = muxcolor.getCRGB(ports[0]); //update
                        drive(-slow); //drive toward white line (backwards)
                    }
                }
            }
        }*/

        drive(0);

        //drive sideways to the right
        if((1000*odsr.getLightDetected()) < rightods){
            while((1000*odsr.getLightDetected()) < rightods && opModeIsActive()){
                telemetry.addData("right ods", 1000*odsr.getLightDetected());
                telemetry.update();
                leftfront.setPower(speed);
                rightfront.setPower(-speed);
                leftback.setPower(-speed);
                rightback.setPower(speed);
            }
        }
        drive(0);

        if((1000*odsr.getLightDetected()) > rightods){
            while((1000*odsr.getLightDetected()) > rightods && opModeIsActive()){
                telemetry.addData("right ods", 1000*odsr.getLightDetected());
                telemetry.update();
                drive(slow);
            }
        }
        drive(0);

        drive(0);

        //update color sensor
        crgb3 = muxcolor.getCRGB(ports[3]);

        //drive until cs2 sees red
        if(isBlue(crgb3,bluelowerbound3,upperbound) || isGrey(crgb3,redlowerbound3,bluelowerbound3,upperbound)){
            while((isBlue(crgb3,bluelowerbound3,upperbound) || isGrey(crgb3,redlowerbound3,bluelowerbound3,upperbound)) && opModeIsActive()){
                crgb3 = muxcolor.getCRGB(ports[3]);
                drive(-slow);
            }
        }
        drive(0);

        //push button
        if(opModeIsActive()) {
            rightpusher.setPosition(Max_pos);
            sleep(2000);
            rightpusher.setPosition(Min_pos);
        }

        //drive sideways to the left
        if((1000*odsr.getLightDetected()) > rightods){
            while((1000*odsr.getLightDetected()) > rightods && opModeIsActive()){
                leftfront.setPower(-speed);
                rightfront.setPower(speed);
                leftback.setPower(speed);
                rightback.setPower(-speed);
            }
        }
        drive(0);

        /*
        //drive forwards
        if(opModeIsActive()){
            drive(-speed);
            sleep(2000);
        }

        drive(0);

        //update color sensor values
        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        //drive forward until one sensor sees a color
        if(isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound)){
            while(isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound) && opModeIsActive()){
                telemetry.addLine("I see grey");
                telemetry.update();
                crgb0 = muxcolor.getCRGB(ports[0]);
                crgb1 = muxcolor.getCRGB(ports[1]);
                drive(-1.5*slow);
            }
        } else {
            drive(0);
        }

        if(opModeIsActive()) {
            leftfront.setPower(speed);
            rightfront.setPower(-speed);
            leftback.setPower(-speed);
            rightback.setPower(speed);
            sleep(400);
        }

        crgb3 = muxcolor.getCRGB(ports[3]);

        //drive until cs2 sees red
        if(isBlue(crgb3,bluelowerbound3,upperbound) || isGrey(crgb3,redlowerbound3,bluelowerbound3,upperbound)){
            while((isBlue(crgb3,bluelowerbound3,upperbound) || isGrey(crgb3,redlowerbound3,bluelowerbound3,upperbound)) && opModeIsActive()){
                telemetry.addLine("I don't see red");
                telemetry.update();
                crgb3 = muxcolor.getCRGB(ports[3]);
                drive(1.25*slow);
            }
        }
        drive(0);

        //push button
        if(opModeIsActive()) {
            rightpusher.setPosition(Max_pos);
            sleep(1000);
            rightpusher.setPosition(Min_pos);
        }*/
    }

    /**
     * Has the robot move all of its motors at one speed. It cannot turn, but it can move fowards, backwards, and stop.
     *
     * @param speed    desired speed of the motors
     */
    public void drive(double speed){
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
    }

    /**
     * Based on predetermined bounds for the color red, decides if the color sensor is sensing a red line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color red
     * @param higherbound    predetermined higher bound for the color red
     */
    public boolean isRed(int[] colorarray, int lowerbound, int higherbound){
        telemetry.addData("Red", colorarray[1]);
        telemetry.update();
        return(colorarray[1] > lowerbound && colorarray[1] < higherbound);
    }

    /**
     * Based on predetermined bounds for the color blue, decides if the color sensor is sensing a blue line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color blue
     * @param higherbound    predetermined higher bound for the color blue
     */
    public boolean isBlue(int[] colorarray, int lowerbound, int higherbound){
        return(colorarray[3] > lowerbound && colorarray[3] < higherbound);
    }

    /**
     * Based on predetermined bounds for the color white, decides if the color sensor is sensing a white line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param bound    predetermined lower bound for the color white (since white is the highest color we sensed, we have no need for a higher bound)
     */
    public boolean isWhite(int[] colorarray, int bound){
        return(colorarray[1] > bound || colorarray[2] > bound || colorarray[3] > bound);
    }

    /**
     * Based on whether it is seeing non-grey colors. If it is, then it is not grey. Otherwise, it is grey.
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerboundred    predetermined lower bound for the color red
     * @param lowerboundblue    predetermined lower bound for the color blue
     * @param whitebound    predetermined higher bound for the colors, which is also the bound for white
     */
    public boolean isGrey(int[] colorarray, int lowerboundred, int lowerboundblue, int whitebound){
        return(!isRed(colorarray,lowerboundred,whitebound) &&
                !isBlue(colorarray,lowerboundblue,whitebound) &&
                !isWhite(colorarray,whitebound));
    }
}