package org.firstinspires.ftc.teamcode;

/**
 * MIT License
 * Copyright (c) 2016 Chris D
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.I2cAddr;
import com.qualcomm.robotcore.hardware.I2cDevice;
import com.qualcomm.robotcore.hardware.I2cDeviceSynch;
import com.qualcomm.robotcore.hardware.I2cDeviceSynchImpl;

/**
 * Created by Sarah McLaughlin on 12/6/16
 */
public class MultiplexerDevice {

    // Registers
    static final int enable = 0x80;
    static final int atime = 0x81;
    static final int control = 0x8F;
    static final int id = 0x92;
    static final int status = 0x93;
    static final int cdatal = 0x94;

    // Default I2C address for multiplexer. The address can be changed to any
    // value from 0x70 to 0x77, so this line would need to be changed if a
    // non-default address is to be used.
    static final I2cAddr muxaddress = new I2cAddr(0x70);
    private I2cDevice mux;
    private I2cDeviceSynch muxreader;

    // Only one color sensor is needed in code as the multiplexer switches
    // between the physical sensors
    private byte[] adacache;
    // I2C address for color sensor
    static final I2cAddr adaaddress = new I2cAddr(0x29);
    private I2cDevice ada;
    private I2cDeviceSynch adareader;

    private int[] sensorports;

    public static int GAIN_1X =  0x00;
    public static int GAIN_4X =  0x01;
    public static int GAIN_16X = 0x02;
    public static int GAIN_60X = 0x03;

    /**
     * Initializes Adafruit color sensors on the specified ports of the I2C multiplexer.
     *
     * @param hardwareMap  hardwareMap from OpMode
     * @param muxName      Configuration name of I2CDevice for multiplexer
     * @param colorName    Configuration name of I2CDevice for color sensor
     * @param ports        Out ports on multiplexer with color sensors attached
     * @param milliSeconds Integration time in milliseconds
     * @param gain         Gain (GAIN_1X, GAIN_4X, GAIN_16X, GAIN_60X)
     */
    public MultiplexerDevice(HardwareMap hardwareMap, String muxName, String colorName,
                              int[] ports, double milliSeconds, int gain) {
        sensorports = ports;

        mux = hardwareMap.i2cDevice.get(muxName);
        muxreader = new I2cDeviceSynchImpl(mux, muxaddress, false);
        muxreader.engage();

        // Loop over the ports activating each color sensor
        for (int i = 0; i < sensorports.length; i++) {
            // Write to given output port on the multiplexer
            muxreader.write8(0x0, 1 << sensorports[i], true);

            ada = hardwareMap.i2cDevice.get(colorName);
            adareader = new I2cDeviceSynchImpl(ada, adaaddress, false);
            adareader.engage();

            final int time = integrationByte(milliSeconds);
            adareader.write8(enable, 0x03, true);  // Power on and enable ADC
            adareader.read8(id);                   // Read device ID
            adareader.write8(control, gain, true); // Set gain
            adareader.write8(atime, time, true);   // Set integration time
        }
    }

    /**
     * Set the integration time on all the color sensors
     * @param milliSeconds Time in millseconds
     */
    public void setIntegrationTime(double milliSeconds) {
        int val = integrationByte(milliSeconds);

        for (int i = 0; i < sensorports.length; i++) {
            muxreader.write8(0x0, 1 << sensorports[i], true);
            adareader.write8(atime, val, true);
        }
    }

    private int integrationByte(double milliSeconds) {
        int count = (int)(milliSeconds/2.4);
        if (count<1)    count = 1;   // Clamp the time range
        if (count>256)  count = 256;
        return (256 - count);
    }

    // Un-needed?
    public void startPolling() {
        for (int i = 0; i < sensorports.length; i++) {
            muxreader.write8(0x0, 1 << sensorports[i], true);
            adareader.read8(status);
        }
    }

    /**
     * Retrieve the color read by the given color sensor
     *
     * @param port Port on multiplexer of given color sensor
     * @return Array containing the Clear, Red, Green, and Blue color values
     */
    public int[] getCRGB(int port) {
        // Write to I2C port on the multiplexer
        muxreader.write8(0x0, 1 << port, true);

        // Read color registers
        adacache = adareader.read(cdatal, 8);

        // Combine high and low bytes
        int[] crgb = new int[4];
        for (int i=0; i<4; i++) {
            crgb[i] = (adacache[2*i] & 0xFF) + (adacache[2*i+1] & 0xFF) * 256;
        }
        return crgb;
    }
}