package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;

/*
 * Created by Sarah and Annelise.
 */

@TeleOp (name = "Teleop", group = "teleop")
public class Teleop111516 extends LinearOpMode {

    //declaring and naming motor variables
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;
    DcMotor ballflicker;
    DcMotor cam;

    Servo leftpusher;
    Servo rightpusher;
    Servo stopper;
    Servo stopperer;

    TouchSensor ts;
    static final double Max_pos = Servo.MAX_POSITION;     // Maximum position
    static final double Min_pos = 0.3;     // Minimum position

    public void runOpMode() throws InterruptedException {
        //configure hardware map & reverse right motor directions
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        ballflicker = hardwareMap.dcMotor.get("ballflicker");
        cam = hardwareMap.dcMotor.get("cam");
        rightpusher = hardwareMap.servo.get("rightpusher");
        leftpusher = hardwareMap.servo.get("leftpusher");
        stopper = hardwareMap.servo.get("stopper");
        stopperer = hardwareMap.servo.get("stopperer");
        ts = hardwareMap.touchSensor.get("touchsensor");

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE);
        rightback.setDirection(DcMotorSimple.Direction.REVERSE);
        cam.setDirection(DcMotorSimple.Direction.REVERSE);


        double threshold = 0.2;
        double scale = 1;
        double sidewayspeed = 1;

        waitForStart();

        while(opModeIsActive()) {
            stopperer.setPosition(0.5);

            //make leftY and rightY variables linked to gamepad values
            double leftY1 = -gamepad1.left_stick_y;
            double rightY1 = -gamepad1.right_stick_y;

            //make speed variables for both wheels
            double speedleft = 1/*scale * leftY1*/;
            double speedright = 1/*scale * rightY1*/;

            //display touch sensor
            telemetry.addData("Touch Sensor", ts.isPressed());
            telemetry.update();

            //allows one ball to be loaded at a time
            if (!ts.isPressed() && gamepad2.x){
                stopper.setPosition(0.0);
            } else if (ts.isPressed() && !gamepad2.x){
                stopper.setPosition(0.0);
            } else {
                stopper.setPosition(0.7);
            }

            //code to drive the robot using tank drive
            if (leftY1 >= threshold/* || leftY1 <= -threshold*/) {
                leftfront.setPower(speedleft);
                leftback.setPower(speedleft);
            } else if (leftY1 <= -threshold){
                leftfront.setPower(-speedleft);
                leftback.setPower(-speedleft);
            } else if (!gamepad1.left_bumper && !gamepad1.right_bumper){
                leftfront.setPower(0);
                leftback.setPower(0);
            }
            if(rightY1 >= threshold/* || rightY1 <= -threshold*/){
                rightfront.setPower(speedright);
                rightback.setPower(speedright);
            } else if (rightY1 <= -threshold){
                rightfront.setPower(-speedleft);
                rightback.setPower(-speedleft);
            } else if (!gamepad1.left_bumper && !gamepad1.right_bumper){
                rightfront.setPower(0);
                rightback.setPower(0);
            }

            //code to drive the robot using mecanum wheels
            if ((rightY1 < threshold) && (rightY1 > -threshold) && (leftY1 < threshold) && (leftY1 > -threshold)) {
                if (gamepad1.left_bumper) {
                    leftfront.setPower(-sidewayspeed);
                    rightfront.setPower(sidewayspeed);
                    leftback.setPower(sidewayspeed);
                    rightback.setPower(-sidewayspeed);
                } else if (gamepad1.right_bumper) {
                    leftfront.setPower(sidewayspeed);
                    rightfront.setPower(-sidewayspeed);
                    leftback.setPower(-sidewayspeed);
                    rightback.setPower(sidewayspeed);
                } else {
                    leftfront.setPower(0);
                    rightfront.setPower(0);
                    leftback.setPower(0);
                    rightback.setPower(0);
                }
            }

            //ballflicker
            if (gamepad2.dpad_up){
                ballflicker.setPower(1);
            } else if (gamepad2.dpad_down){
                ballflicker.setPower(0.5);
            } else if (gamepad2.left_stick_button){
                ballflicker.setPower(-0.5);
            } else {
                ballflicker.setPower(0);
            }

            //move buttonpushers
                //moves to the left
            if (gamepad2.left_bumper){
                leftpusher.setPosition(Max_pos);
                //moves to the right
            } else if (gamepad2.right_bumper){
                rightpusher.setPosition(Max_pos);
            } else {
                leftpusher.setPosition(0.3);
                rightpusher.setPosition(0.3);
            }

            //triggers cam
            if (gamepad2.a){
               cam.setPower(1);
            } else if (gamepad2.y) {
                cam.setPower(0.5);
            } else if (gamepad2.b){
                cam.setPower(-0.5);
            } else {
                cam.setPower(0);
            }
        }
    }
}