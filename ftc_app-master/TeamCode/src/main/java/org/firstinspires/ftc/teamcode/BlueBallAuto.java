package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;

/**
 * Created by Iris Li on 2/16/17
 */
@Autonomous (name = "Blue Ball Autonomous", group = "Autonomouses")
public class BlueBallAuto extends LinearOpMode{

    MultiplexerDevice muxcolor;
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;
    DcMotor ballflicker;
    DcMotor cam;

    Servo stopper;

    TouchSensor ts;

    int[] ports = {0, 1, 2, 3};

    @Override
    public void runOpMode() throws InterruptedException {
        leftback = hardwareMap.dcMotor.get("leftback");
        leftfront = hardwareMap.dcMotor.get("leftfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        ballflicker = hardwareMap.dcMotor.get("ballflicker");
        cam = hardwareMap.dcMotor.get("cam");
        stopper = hardwareMap.servo.get("stopper");
        ts = hardwareMap.touchSensor.get("touchsensor");
        cam.setDirection(DcMotorSimple.Direction.REVERSE);

        rightfront.setDirection(DcMotorSimple.Direction.REVERSE); //*** NEED TO UNCOMMENT ON HARDWARE ROBOT***///
        rightback.setDirection(DcMotorSimple.Direction.REVERSE); //*** NEED TO UNCOMMENT ON HARDWARE ROBOT***///

        int milliSeconds = 48;
        muxcolor = new MultiplexerDevice(hardwareMap, "mux", "ada", ports, milliSeconds,
                MultiplexerDevice.GAIN_16X);

        //threshold values for 48 milliSeconds
        int upperbound = SensorVal.hwhitebound;
        int redlowerbound0 = SensorVal.hredbound0;
        int redlowerbound1 = SensorVal.hredbound1;
        int redlowerbound2 = SensorVal.hredbound2;
        int bluelowerbound0 = SensorVal.hbluebound0;
        int bluelowerbound1 = SensorVal.hbluebound1;
        int bluelowerbound2 = SensorVal.hbluebound2;

        //create an array of the different crgb values
        int[] crgb0;  //left
        int[] crgb1;  //right

        double speed = SensorVal.speed12_1;
        double slow = SensorVal.slow12_1;

        stopper.setPosition(0.5);

        waitForStart();

        crgb0 = muxcolor.getCRGB(ports[0]);
        crgb1 = muxcolor.getCRGB(ports[1]);

        if(isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound)){
            while (opModeIsActive() && isGrey(crgb0,redlowerbound0,bluelowerbound0,upperbound) && isGrey(crgb1,redlowerbound1,bluelowerbound1,upperbound)) {
                crgb0 = muxcolor.getCRGB(ports[0]);
                crgb1 = muxcolor.getCRGB(ports[1]);
                drive(-speed / 1.5);
            }
        }

        drive(0);

        if(opModeIsActive()){
            drive(speed);
            sleep(800);
        }
        drive(0);


        //moves cam while ts is pressed
        if(!ts.isPressed()){
            while(!ts.isPressed() && opModeIsActive()){
                cam.setPower(1);
            }
        }
        cam.setPower(0);
        stopper.setPosition(0.0);

        if(ts.isPressed()){
            while(ts.isPressed() && opModeIsActive()){
                ballflicker.setPower(.9);
            }
        }
        sleep(300);
        ballflicker.setPower(0);
        stopper.setPosition(0.5);

        if(!ts.isPressed()){
            while(!ts.isPressed() && opModeIsActive()){
                cam.setPower(1);
            }
        }
        cam.setPower(0);

        drive(-speed);
        sleep(600);
        drive(0);
        
    }

    /**
     * Has the robot move all of its motors at one speed. It cannot turn, but it can move fowards, backwards, and stop.
     *
     * @param speed    desired speed of the motors
     */
    public void drive(double speed){
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
    }

    /**
     * Based on predetermined bounds for the color red, decides if the color sensor is sensing a red line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color red
     * @param higherbound    predetermined higher bound for the color red
     */
    public boolean isRed(int[] colorarray, int lowerbound, int higherbound){
        return(colorarray[1] > lowerbound && colorarray[1] < higherbound);
    }

    /**
     * Based on predetermined bounds for the color blue, decides if the color sensor is sensing a blue line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerbound    predetermined lower bound for the color blue
     * @param higherbound    predetermined higher bound for the color blue
     */
    public boolean isBlue(int[] colorarray, int lowerbound, int higherbound){
        return(colorarray[3] > lowerbound && colorarray[3] < higherbound);
    }

    /**
     * Based on predetermined bounds for the color white, decides if the color sensor is sensing a white line or not
     *
     * @param colorarray   array the color sensor stores its values in
     * @param bound    predetermined lower bound for the color white (since white is the highest color we sensed, we have no need for a higher bound)
     */
    public boolean isWhite(int[] colorarray, int bound){
        return(colorarray[1] > bound || colorarray[2] > bound || colorarray[3] > bound);
    }

    /**
     * Based on whether it is seeing non-grey colors. If it is, then it is not grey. Otherwise, it is grey.
     *
     * @param colorarray   array the color sensor stores its values in
     * @param lowerboundred    predetermined lower bound for the color red
     * @param lowerboundblue    predetermined lower bound for the color blue
     * @param whitebound    predetermined higher bound for the colors, which is also the bound for white
     */
    public boolean isGrey(int[] colorarray, int lowerboundred, int lowerboundblue, int whitebound){
        return(!isRed(colorarray,lowerboundred,whitebound) &&
                !isBlue(colorarray,lowerboundblue,whitebound) &&
                !isWhite(colorarray,whitebound));
    }
}
