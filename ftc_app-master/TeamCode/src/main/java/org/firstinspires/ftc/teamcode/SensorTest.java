package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;

/**
 * Created by Sarah McLaughlin on 12/6/16
 */

@TeleOp (name = "Sensor Test", group = "Teleop")
public class SensorTest extends LinearOpMode {

    OpticalDistanceSensor odsl;
    OpticalDistanceSensor odsr;

    MultiplexerDevice muxcolor;
    int[] ports = {0, 1, 2, 3};

    public void runOpMode() throws InterruptedException{
        int milliSeconds = 48;
        telemetry.addLine("Debug");
        muxcolor = new MultiplexerDevice(hardwareMap, "mux", "ada", ports, milliSeconds,
                MultiplexerDevice.GAIN_16X);

        odsl = hardwareMap.opticalDistanceSensor.get("odsl");
        odsr = hardwareMap.opticalDistanceSensor.get("odsr");

        waitForStart();
        muxcolor.startPolling();
        while(opModeIsActive()){
            for (int i = 0; i < ports.length; i++) {
                int[] crgb = muxcolor.getCRGB(ports[i]);

                telemetry.addLine("Sensor " + ports[i]);
                telemetry.addData("CRGB", "%5d %5d %5d %5d", crgb[0], crgb[1], crgb[2], crgb[3]);
            }

            telemetry.addData("Raw L", 1000*odsl.getRawLightDetected());
            telemetry.addData("Normal L", 1000*odsl.getLightDetected());
            telemetry.addData("Raw R", 1000*odsr.getRawLightDetected());
            telemetry.addData("Normal R", 1000*odsr.getLightDetected());

            telemetry.update();
        }
    }
}
