package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by sarahmclaughlin on 2/7/17.
 */
@TeleOp (name = "Servo Reset", group = "teleop")
public class ServoSet extends LinearOpMode{
    Servo leftpusher;
    Servo rightpusher;

    public void runOpMode() throws InterruptedException{
        leftpusher = hardwareMap.servo.get("leftpusher");
        rightpusher = hardwareMap.servo.get("rightpusher");

        waitForStart();

        while(opModeIsActive()){
            leftpusher.setPosition(0.3);
            rightpusher.setPosition(0.3);
        }
    }
}
