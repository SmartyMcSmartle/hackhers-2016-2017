package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/*
 * Created by Sarah and Annelise.
 */

@TeleOp (name = "Software robot", group = "teleop")
public class Teleop102516 extends LinearOpMode {

    //declaring and naming motor variables
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    public void runOpMode() throws InterruptedException {
        //configure hardware map & reverse right motor directions
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightback.setDirection(DcMotor.Direction.REVERSE);

        double threshold = 0.2;
        double scale = 1;
        double sidewayspeed = 1;

        waitForStart();

        while(opModeIsActive()) {
            //make leftY and rightY variables linked to gamepad vals
            double leftY1 = -gamepad1.left_stick_y;
            double rightY1 = -gamepad1.right_stick_y;

            //make speed variables for both wheels
            double speedleft = 1;
            double speedright = 1;

            //display speed values
            telemetry.addData("Left Power", speedleft);
            telemetry.addData("Right Power", speedright);

            //code to drive the robot using tank drive
            if (leftY1 >= threshold) {
                leftfront.setPower(speedleft);
                leftback.setPower(speedleft);
            } else if (leftY1 <= -threshold){
                leftfront.setPower(-speedleft);
                leftback.setPower(-speedleft);
            } else if (!gamepad1.left_bumper && !gamepad1.right_bumper){
                leftfront.setPower(0);
                leftback.setPower(0);
            }
            if(rightY1 >= threshold){
                rightfront.setPower(speedright);
                rightback.setPower(speedright);
            } else if (rightY1 <= -threshold){
                rightfront.setPower(-speedleft);
                rightback.setPower(-speedleft);
            } else if (!gamepad1.left_bumper && !gamepad1.right_bumper){
                rightfront.setPower(0);
                rightback.setPower(0);
            }

            //code to drive the robot using mecanum wheels
            if ((rightY1 < threshold) && (rightY1 > -threshold) && (leftY1 < threshold) && (leftY1 > -threshold)) {
                if (gamepad1.left_bumper) {
                    leftfront.setPower(-sidewayspeed);
                    rightfront.setPower(sidewayspeed);
                    leftback.setPower(sidewayspeed);
                    rightback.setPower(-sidewayspeed);
                } else if (gamepad1.right_bumper) {
                    leftfront.setPower(sidewayspeed);
                    rightfront.setPower(-sidewayspeed);
                    leftback.setPower(-sidewayspeed);
                    rightback.setPower(sidewayspeed);
                } else {
                    leftfront.setPower(0);
                    rightfront.setPower(0);
                    leftback.setPower(0);
                    rightback.setPower(0);
                }
            }
        }
    }
}
